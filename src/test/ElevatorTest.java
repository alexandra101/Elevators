package test;

import edu.elevators.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ElevatorTest {

    @DataProvider(name = "initTest")
    public Object[][] initTest() throws OutOfRangeException {

        ElevatorBuilder elevatorBuilder =  new ElevatorBuilder();
        PersonBuilder personBuilder = new PersonBuilder();

        ArrayList<Elevator> elevators1 = new ArrayList<>();
        ArrayList<Elevator> elevators2 = new ArrayList<>();
        ArrayList<Elevator> elevators3 = new ArrayList<>();
        ArrayList<Elevator> elevators4 = new ArrayList<>();
        ArrayList<Elevator> elevators5 = new ArrayList<>();
        ArrayList<Elevator> elevators6 = new ArrayList<>();
        ArrayList<Elevator> elevators7 = new ArrayList<>();

        elevators1.add(elevatorBuilder.buildElevator(100, 2, Elevator.State.DOWN, 100));
        elevators1.add(elevatorBuilder.buildElevator(1, 2, Elevator.State.UP, 100));

        elevators2.add(elevatorBuilder.buildElevator(100, 2, Elevator.State.DOWN, 100));
        elevators2.add(elevatorBuilder.buildElevator(5, 3, Elevator.State.DOWN, 100));
        elevators2.add(elevatorBuilder.buildElevator(7, 4, Elevator.State.DOWN, 100));

        elevators3.add(elevatorBuilder.buildElevator(100, 2, Elevator.State.DOWN, 100));
        elevators3.add(elevatorBuilder.buildElevator(5, 4, Elevator.State.DOWN, 100));
        elevators3.add(elevatorBuilder.buildElevator(2, 4, Elevator.State.DOWN, 100));


        elevators4.add(elevatorBuilder.buildElevator(100, 99, Elevator.State.DOWN, 100));
        elevators4.add(elevatorBuilder.buildElevator(2, 96, Elevator.State.UP, 100));
        elevators4.add(elevatorBuilder.buildElevator(95, 94, Elevator.State.DOWN, 100));
        elevators4.add(elevatorBuilder.buildElevator(10, 1, Elevator.State.DOWN, 100));


        elevators5.add(elevatorBuilder.buildElevator(6, 3, Elevator.State.DOWN, 100));
        elevators5.add(elevatorBuilder.buildElevator(4, 2, Elevator.State.DOWN, 100));

        elevators6.add(elevatorBuilder.buildElevator(7, 9, Elevator.State.UP, 10));
        elevators6.add(elevatorBuilder.buildElevator(3, 4, Elevator.State.UP, 10));
        elevators6.add(elevatorBuilder.buildElevator(10, 5, Elevator.State.DOWN, 10));


        elevators7.add(elevatorBuilder.buildElevator(7, 9, Elevator.State.UP, 10));
        elevators7.add(elevatorBuilder.buildElevator(6, 8, Elevator.State.UP, 10));
        elevators7.add(elevatorBuilder.buildElevator(2, 3, Elevator.State.UP, 10));
        elevators7.add(elevatorBuilder.buildElevator(4, 3, Elevator.State.DOWN, 10));

        return new Object[][]{

                {personBuilder.buildPerson(3, Person.Direction.UP, 100), elevators1, 1},
                {personBuilder.buildPerson(4, Person.Direction.DOWN, 100), elevators2, 1},
                {personBuilder.buildPerson(4, Person.Direction.DOWN, 100), elevators3, 1},
                {personBuilder.buildPerson(96, Person.Direction.DOWN, 100), elevators4, 1},
                {personBuilder.buildPerson(5, Person.Direction.DOWN, 100), elevators5, 0},
                {personBuilder.buildPerson(5, Person.Direction.UP, 10), elevators6, 2 },
                {personBuilder.buildPerson(5, Person.Direction.UP, 10), elevators7, 3}

        };
    }


    @BeforeClass
    private void addDate() throws IOException {
        Date date = new Date();
        WorkWithFile workWithFile = new WorkWithFile();
        workWithFile.write(String.valueOf(date));
    }

    @Test(dataProvider = "initTest")
    public  void testElevator(Person person, List<Elevator> elevators, int rightElevator) throws IOException {
        long startTime = System.nanoTime();
        Assert.assertEquals(rightElevator, elevators.indexOf(person.findBestElevator(elevators, 100)));
        long endTime = System.nanoTime() - startTime;

        WorkWithFile workWithFile = new WorkWithFile();
        workWithFile.write(workWithFile.concatPersonInfo(person));
        workWithFile.write(workWithFile.concatElevatorInfo(elevators));
        workWithFile.write("Time: ".concat(String.valueOf(endTime)));

    }




}
