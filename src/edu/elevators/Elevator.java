package edu.elevators;

public class Elevator {


    private int currentFloor;
    private int destFloor;
    public enum State {UP, DOWN, IDLE}
    private State state;

    public Elevator() {
        currentFloor = 0;
        destFloor = 0;
        state = State.IDLE;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getDestFloor() {

        return destFloor;
    }

    public void setDestFloor(int destFloor) {
        this.destFloor = destFloor;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

}
