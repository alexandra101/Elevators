package edu.elevators;

import java.util.ArrayList;
import java.util.List;

public class Service {


    private  int priorityWorse;
    private  int priorityWorst;
    private int currentFloor;


    public Service(){
        priorityWorse = 1;
        priorityWorst = 2;
        currentFloor=0;

    }

    public Service(int personFloor, int maxFloor){
        priorityWorse = maxFloor;
        priorityWorst = 2 * maxFloor;
        this.currentFloor=personFloor;

    }
    private int findMinDistance(ArrayList<Integer> arrayCoeff) {
        int min = arrayCoeff.get(0);
        int index = 0;
        for (int i = 1; i < arrayCoeff.size(); i++) {
            if (min >= arrayCoeff.get(i)) {
                min = arrayCoeff.get(i);
                index = i;
            }
        }
        return index;
    }

   Elevator goUp(List<Elevator> elevators) {
        ArrayList<Integer> arrayCoeff = new ArrayList<>();
        for (int i = 0; i < elevators.size(); i++) {

            //the elevator is on the same floor
            if ((elevators.get(i).getState() == Elevator.State.IDLE) && (elevators.get(i).getCurrentFloor() == currentFloor)) {
                return elevators.get(i);

            }
            //the elevator is lower and goes up
            else if ((elevators.get(i).getState() == Elevator.State.UP) && (elevators.get(i).getCurrentFloor() < currentFloor)) {

                arrayCoeff.add(Math.abs(currentFloor - elevators.get(i).getCurrentFloor()));
            } else

                //the elevator has worse position
                arrayCoeff.add(Math.abs((elevators.get(i).getCurrentFloor() - elevators.get(i).getDestFloor()) + (currentFloor - elevators.get(i).getDestFloor())));

            //the elevator is lower and goes down
            if (elevators.get(i).getDestFloor() < currentFloor) {
                arrayCoeff.set(i, arrayCoeff.get(i) + priorityWorse);
            }

            //the elevator is higher and goes up
            if (elevators.get(i).getDestFloor() > currentFloor && elevators.get(i).getCurrentFloor() > currentFloor) {
                arrayCoeff.set(i, arrayCoeff.get(i) + priorityWorst);
            }

        }

       return elevators.get(findMinDistance(arrayCoeff));
    }

     Elevator goDown(List<Elevator> elevators) {
        ArrayList<Integer> arrayCoeff = new ArrayList<>();
        for (int i = 0; i < elevators.size(); i++) {

            //the elevator is on the same floor
            if ((elevators.get(i).getState() == Elevator.State.IDLE) && (elevators.get(i).getCurrentFloor() == currentFloor)) {
                return elevators.get(i);

            }
            //the elevator is higher and goes down
            else if ((elevators.get(i).getState() == Elevator.State.DOWN) && (elevators.get(i).getCurrentFloor() > currentFloor)) {

                arrayCoeff.add(Math.abs(currentFloor - elevators.get(i).getCurrentFloor()));
            } else
                arrayCoeff.add(Math.abs((elevators.get(i).getCurrentFloor() - elevators.get(i).getDestFloor()) + (currentFloor - elevators.get(i).getDestFloor())));
            if (elevators.get(i).getDestFloor() > currentFloor) {
                arrayCoeff.set(i, arrayCoeff.get(i) + priorityWorse);
            }
            if (elevators.get(i).getDestFloor() < currentFloor && elevators.get(i).getCurrentFloor() < currentFloor) {
                arrayCoeff.set(i, (int) (arrayCoeff.get(i) + priorityWorst));
            }

        }

       return elevators.get(findMinDistance(arrayCoeff));
    }

}
