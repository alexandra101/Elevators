package edu.elevators;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class WorkWithFile {
    private static final String FILE_PATH="src/test/testTime.log";
    public void write(String str) throws IOException {

       File  file = new File(FILE_PATH);

        try(FileWriter fileWriter=new FileWriter(file, true)) {

            fileWriter.write(str+"\n");
            fileWriter.flush();

        }
    }


    public String concatPersonInfo(Person person){
        StringBuilder stringBuilder = new StringBuilder("Person info:");
        stringBuilder.append(person.getCurrentFloor());
        stringBuilder.append(", ");
        stringBuilder.append(person.getDirection());
        stringBuilder.append("; ");
        return String.valueOf(stringBuilder);

    }


    public String concatElevatorInfo(List<Elevator> elevators){

        StringBuilder stringBuilder = new StringBuilder("Elevators info: ");
        for (Elevator elevator: elevators){
            stringBuilder.append("[");
            stringBuilder.append(elevator.getCurrentFloor());
            stringBuilder.append(",");
            stringBuilder.append(elevator.getDestFloor());
            stringBuilder.append("]; ");
        }
        return String.valueOf(stringBuilder);
    }
}
