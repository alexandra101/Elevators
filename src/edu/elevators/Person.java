package edu.elevators;

import java.util.List;

public class Person {

    private int currentFloor;


  public   enum Direction{UP, DOWN}

    private Direction direction;



    public Person() {
        currentFloor=0;
        direction=Direction.UP;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }




    public Elevator findBestElevator(List<Elevator> elevators, int maxFloor) {
        Service service = new Service(currentFloor, maxFloor);



        if (direction == Direction.UP) {

           return service.goUp(elevators);
        } else if (direction == Direction.DOWN) {

            return service.goDown(elevators);
        }

        return null;
    }
}
