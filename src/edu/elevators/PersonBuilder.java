package edu.elevators;

public class PersonBuilder {

    public Person buildPerson(int currentFloor, Person.Direction direction, int maxFloor) throws OutOfRangeException {
        Person person = new Person();

       person.setCurrentFloor(currentFloor);
        if (currentFloor == 0) {
            person.setDirection(Person.Direction.UP);
        } else if (currentFloor == maxFloor) {
            person.setDirection(Person.Direction.DOWN);
        } else if ((currentFloor<0)||currentFloor>maxFloor) throw new OutOfRangeException("Person floor is not in the range! ");

            person.setDirection(direction);
        return person;
    }

    }


