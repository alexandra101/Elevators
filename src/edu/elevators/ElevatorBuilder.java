package edu.elevators;

public class ElevatorBuilder {


   public Elevator buildElevator(int currentFloor, int destFloor, Elevator.State state, int maxFloor) throws OutOfRangeException {
       Elevator elevator = new Elevator();

       if (currentFloor >= 0 && currentFloor <= maxFloor) {
           elevator.setCurrentFloor(currentFloor);
       } else throw new OutOfRangeException("The elevator current floor is not in the range! ");

       if (destFloor >= 0 && destFloor <= maxFloor) {
           elevator.setDestFloor(destFloor);
       } else throw new OutOfRangeException("The elevator destination floor is not in the range! ");

       if (currentFloor == 0) {
           elevator.setState(Elevator.State.UP);
       } else if (currentFloor == maxFloor) {
           elevator.setState(Elevator.State.DOWN);
       } else
           elevator.setState(state);

       return elevator;
   }
}
